resource "yandex_lb_target_group" "foo" {
  name      = "lb-group"
  target {
    subnet_id = "${yandex_vpc_subnet.default.id}"
    address   = "192.168.101.11"
  }
}

resource "yandex_lb_network_load_balancer" "foo" {
  name = "network-lb"
  listener {
    name = "network-listener"
    port = 8080
    external_address_spec {
      ip_version = "ipv4"
    }
  }
  attached_target_group {
    target_group_id = "${yandex_lb_target_group.foo.id}"
    healthcheck {
      name = "network-test"
        http_options {
          port = 8080
          path = "/"
        }
    }
  }
}
# Нету на яндексе Терминации SSL