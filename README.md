## Terraform
Создает виртуальную машину на yandex-cloud с сетью и Network Load Balancer на порт 8080.  
В файле [meta.txt](https://gitlab.com/keeper5211/work-test/-/tree/main/terraform/meta.txt) необходимо прописать логин и ключ ssh по которому можно будет подключиться на виртуальную машину.  
Запуск terraform осуществляется командой:
```
terraform apply
```
Удалить все изменения сделанные terraform можно командой:
```
terraform destroy
```
## Ansible
Устанавливает Docker на созданную терраформом виртуальную машину, а так же производит установку gitlab-runner и подключает его к аккаунту на gitlab.  
Перед запуском ansible необходимо прописать token в [all.yml](https://gitlab.com/keeper5211/work-test/-/blob/main/ansible/group_vars/all.yml), а так же ip адрес виртуальной  машины и путь к ssh ключу в [hosts](https://gitlab.com/keeper5211/work-test/-/blob/main/ansible/inventory/hosts).
Запуск плейбука осуществляется командой:
```
ansible-playbook docker.yml -i inventory/hosts
```
## GitLab
При изменении в репозитории происходит удаление контейнера и образа на Gitlab-runner с последующей пересборкой контейнера и запуском.
